import { Injectable } from '@angular/core';
import * as RecordRTC from 'recordrtc';
import * as moment from 'moment';
import { Observable, Subject,interval, of, observable } from 'rxjs';
 

interface RecordedAudioOutput {
  blob: Blob;
  title: string;
}

@Injectable()
export class AudioRecordingService {
 

  private stream;
  private recorder;
  private interval;
  private startTime;
  private _recorded = new Subject<RecordedAudioOutput>();
  private _recordingTime = new Subject<string>();
  private _recordingTimeSeconds = new Subject<moment.Duration>();
  private _remaningTime = new Subject<string>();
  private _recordingFailed = new Subject<string>();


  getRecordedBlob(): Observable<RecordedAudioOutput> {
    return this._recorded.asObservable();
  }

  getRecordedTime(): Observable<string> {
    return this._recordingTime.asObservable();
  }

  getRecordedTimeSeconds(): Observable<moment.Duration> {
    return this._recordingTimeSeconds.asObservable();
  }

  getRemainingTime():Observable<string> {
    return this._remaningTime.asObservable();
  }

  recordingFailed(): Observable<string> {
    return this._recordingFailed.asObservable();
  }


  startRecording() {
    if (this.recorder) {
      return;
    }
    this._recordingTime.next('00:00');
    this._recordingTimeSeconds.next(moment.duration(0));
    this._remaningTime.next("2:00");
    this.startTimer(120);
    navigator.mediaDevices.getUserMedia({ audio: {echoCancellation:true} }).then(s => {
      this.stream = s;
      this.record();
    }).catch(error => {
      this._recordingFailed.next();
    });

  }

  abortRecording() {
    this.stopMedia();
  }

  private record() {

    this.recorder = new RecordRTC.StereoAudioRecorder(this.stream, {
      type: 'audio',
      mimeType: 'audio/mp3',
      audioBitsPerSecond: 16000,
      numberOfAudioChannels: 1,
      elementClass: 'multi-streams-mixer',
      timeSlice:120
    });

    this.recorder.record();
    this.startTime = moment();
    this.interval = setInterval(
      () => {
        const currentTime = moment();
        const diffTime = moment.duration(currentTime.diff(this.startTime));
        const diffTimeRemain = moment.duration(120*1000-currentTime.diff(this.startTime));
        const time = this.toString(diffTime.minutes()) + ':' + this.toString(diffTime.seconds());
        const timeRemain = this.toString(diffTimeRemain.minutes()) + ':' + this.toString(diffTimeRemain.seconds());
        this._recordingTime.next(time);
        this._recordingTimeSeconds.next(diffTime)
        this._remaningTime.next(timeRemain);
      },
      100
    );
   
  }

  progressbarValue = 100;
  curSec: number = 0;

  startTimer(seconds: number) {
    const time = seconds;
    const timer$ = interval(1000);

    const sub = timer$.subscribe((sec) => {
      this.progressbarValue = 100 - sec * 100 / seconds;
      this.curSec = sec;
      this._remaningTime.next(this.toString(time));
      if (this.curSec === seconds) {
        sub.unsubscribe();
      }
    });
  }

  private toString(value) {
    let val = value;
    if (!value) {
      val = '00';
    }
    if (value < 10) {
      val = '0' + value;
    }
    return val;
  }

  stopRecording() {

    if (this.recorder) {
      this.recorder.stop((blob) => {
        if (this.startTime) {
          const mp3Name = encodeURIComponent('audio_' + new Date().getTime() + '.wav');
          this.stopMedia();
          this._recorded.next({ blob: blob, title: mp3Name });
        }
      }, () => {
        this.stopMedia();
        this._recordingFailed.next();
      });
    }
  }

  private stopMedia() {
    if (this.recorder) {
      this.recorder = null;
      clearInterval(this.interval);
      this.startTime = null;
      if (this.stream) {
        this.stream.getAudioTracks().forEach(track => track.stop());
        this.stream = null;
      }
    }
  }

  


}