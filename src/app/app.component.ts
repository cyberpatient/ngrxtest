import { Component } from '@angular/core';
import { DomSanitizer } from "@angular/platform-browser";
import { AudioRecordingService } from "./audio-recording.service";
import * as moment from 'moment';
import {ViewChild, ElementRef} from '@angular/core';
import { interval } from 'rxjs';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})


 


export class AppComponent {
  @ViewChild('firstNepameInput', { static: true }) epaRef: ElementRef;
  isRecording = false;
  recordedTime = "";
  blobUrl = null;
  recordedFile: any = "";
  filesaved = false;
  step = 0;
  indexExpanded: number = -1;
  buttonDisabled: false;
  remainingTime = "";

  clicked: boolean = false;
  loadingAndSaving: boolean = false;
  myRec: Recordings;
 
  recordArry: any = [];
  instanceName:string;
  component:string;

  progressbarValue = 0;
  curSec: number = 0;
  recordedTimeSeconds: moment.Duration;

 
  setStep(index: number) {
    this.step = index;
  }
  togglePanels(index: number) {
    console.log(index)
    this.indexExpanded = index == this.indexExpanded ? -1 : index;
  }
  nextStep() {
    this.step++;
  }
  panelDisable:boolean=false;

  prevStep() {
    this.step--;
  }
  

 
  private epalist: EPA[]= [
    {
      EPA: "EPA1",
      meetingwith: "Patient",
      epaIsRequired: true,
      cyberScore: 80,
      checklist: [
        "Obtains the appropriate data from the patient (family/caregiver/advocate) for the specific patient encounter",
        "Establishes a rapport with the patient (family/caregiver/advocate)",
        "Performs a physical exam appropriately tailored to the clinical case",
        "Demonstrates specific physical exam skills appropriate to the patient case.",
        "Integrates all these elements along with other sources of information"
      ],
      recordings: {
        file: "",
        created:"",
        caseid:""
      },
      expanded: false
    },
    {
      EPA: "EPA2",
      epaIsRequired: true,
      meetingwith: "Patient Family",
      cyberScore: 80,
      checklist: [
        "Lists diagnostic possibilities by integrating elements from the history, physical examination, and investigative studies",
        "Identifies the major diagnostic possibilities for common clinical presentations",
        "Justifies and prioritizes a most likely diagnosis based on information from his/her clinical assessment",
        "Incorporates major determinants of health for the patient when generating and prioritizing the differential",
        "Balances the tendency to be too all encompassing yet avoids errors of premature closure"
      ],
      recordings: {
        file: "",
        created:"",
        caseid:""
      },
      expanded: false
    },
    {
      EPA: "EPA3",
      epaIsRequired: true,
      meetingwith: "Caretaker",
      cyberScore: 80,
      checklist: [
        "Orders (or decides not to order) tests considering their features and limitations (e.g., reliability, sensitivity, specificity), availability, acceptability for the patient, inherent risks and contribution to a management decision",
        "In case of social implications of positive results, discusses the selection of the tests with patients/family/caregiver/advocate when ordering them (e.g. HIV, pregnancy in an adolescent)",
        "Identifies levels of uncertainty at each step of the diagnostic process and do not over-investigate or underinvestigate",
        "Chooses diagnostic interventions using evidence or best practice/guidelines according to costs and availability of resources taking into consideration the way in which care is organized",
        "Identifies who will be responsible for the follow-up of the test results"
      ],
      recordings: {
        file: "",
        created:"",
        caseid:""
      },
      expanded: false
    },
    {
      EPA: "EPA4",
      epaIsRequired: true,
      meetingwith: "Social worker",
      cyberScore: 80,
      checklist: [
        "Recognizes significant urgent or abnormal results",
        "Distinguishes between common normal variations in results and abnormal results",
        "Formulates an appropriate preliminary opinion about the potential clinical impact of results",
        "Communicates significant results in a timely and appropriate manner to other team members",
        "Summarizes and interprets the meaning of the results to other team members",
        "Communicates results in a clear manner to patients (family/caregiver/advocate)",
        "Seeks help to interpret results when necessary"
      ],
      recordings: {
        file: "",
        created:"",
        caseid:""
      },
      expanded: false
    }
  ];
  
  constructor(
    private audioRecordingService: AudioRecordingService,
    private sanitizer: DomSanitizer
 
  
 
 
  ) {}

  ngOnInit() {
    //TODO
    //Remove this line when testing
    //this.sg["case_id"] = "5e6fc729aa23d900117c1da8";
    this.audioRecordingService.recordingFailed().subscribe(() => {
      this.recordedTime = '';
      this.isRecording = false;
      this.filesaved = false;
    });

    this.audioRecordingService.getRecordedTime().subscribe(time => {
      this.recordedTime = time;
    });

    this.audioRecordingService.getRecordedTimeSeconds().subscribe(time =>{
      this.recordedTimeSeconds = time;
    })

    this.audioRecordingService.getRemainingTime().subscribe(time => {
      this.remainingTime = time;
    });

    this.audioRecordingService.getRecordedBlob().subscribe(data => {
      this.blobUrl = this.sanitizer.bypassSecurityTrustUrl(
        URL.createObjectURL(data.blob)
      );
      //play
      this.recordedFile = data;
      console.log(data);
    });

 
 
 
  }
 
  startRecording() {
    if (!this.blobUrl == null) {
      alert("please save or discard previous recording");
    } else if (!this.isRecording && this.blobUrl == null) {
      this.recordedTime = '';
      this.isRecording = true;
      this.panelDisable=true;
      this.progressbarValue  =  moment.duration(this.recordedTimeSeconds).asSeconds(); 
 
      this.audioRecordingService.startRecording();
    }
  }

  abortRecording() {
    if (this.isRecording) {
      this.isRecording = false;
      this.panelDisable=false;
      this.filesaved = false;
      this.audioRecordingService.abortRecording();
    }
  }

  stopRecording() {
    if (this.isRecording) {
      this.audioRecordingService.stopRecording();
      this.panelDisable=true;
      this.isRecording = false;
      this.recordedTime='';
    }
  }
  getSafeUrl(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }

  saveRecordedData(e) {
    this.clicked = true;

    this.epalist.forEach((x, y) => {
      if (x.EPA == e) {
       
        x.recordings = {
          file: this.blobUrl,
          created: new Date().toDateString(),
          caseid:"aa"
        };
        this.epalist[y] = x;
        this.recordArry.push(x);
        this.isRecording = false;
        this.blobUrl = null;
      }
    });
    this.panelDisable=false;
    this.blobUrl = null;
  }
  panelOpenState: boolean = false;

  togglePanel() {
    this.panelOpenState = !this.panelOpenState;
  }

  submitRecordings() {
 
  }

  clearRecordedData() {
    this.blobUrl = null;
    this.filesaved = true;
    this.panelDisable=false;
    this.recordedTime='';
  }

  checkAllRecording() {
    if (this.recordArry.length > 0) {
      return true;
    } else {
      return false;
    }
  }
  ngOnDestroy(): void {
    this.abortRecording();
  }
}

export class EPA {
  EPA: string;
  meetingwith: string;
  epaIsRequired:  boolean;
  cyberScore: number;
  checklist: string[];
  recordings: {
      file: any,
      created:string,
      caseid:any
    }
    expanded: boolean;
}


interface Recordings {
  caseid: string;
  instanceid: string;
  patientrecord: Blob;
  patientrecordurl: string;
  patientFamilyrecord: Blob;
  patientFamilyrecordurl: string;
  caretakerrecord: Blob;
  caretakerrecordurl: string;
  socialWorkerrecord: Blob;
  socialWorkerurl: string;
}
